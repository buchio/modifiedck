﻿#########################################################################
# コンパイル

  Visual Studio Express 2012 for Windows Desktop と cygwin gccが必要です。
  (Visual C++ 2010 Express Editionでも可)


  VS2012コマンドプロンプト

  > StartMenu
    > All Programs
      > Microsoft Visual Studio 2012
        > Visual Studio Tools
          > VS2012 x86 Native Tools コマンドプロンプト

  を開いて

  cd /d C:\download\ck-3.6.4\src [enter]
  nmake.exe [enter]

  これでコンパイルできます。
  次に ck.con.exe は cygwin gccで作ります。
  cygwinから

  cd /cygdrive/c/download/ck-3.6.4/src [enter]
  windres -o main_con_gcc.res.o -D TARGET=1 -i rsrc.rc [enter]
  gcc -o ck.con.exe -pipe -s -O2 -Wall -static-libgcc main_con_gcc.c main_con_gcc.res.o [enter]

  出来上がった ck.exe ck.con.exe ck.app.dll を /bin にコピーして入れ、
  ck.exeをダブルクリックして起動すれば完成。

#########################################################################
# その他

  文字コード変換テーブル(encoding_table.cpp)は .NET frameworkのデータからC#で生成しています。
    csc.exe gen_enc.cs [enter]
    gen_enc.exe [enter]
