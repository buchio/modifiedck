﻿/*----------------------------------------------------------------------------
 * Copyright 2007-2012  Kazuo Ishii <kish@wb3.so-net.ne.jp>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *--------------------------------------------------------------------------*/
#include "config.h"

#include <cmath>
#pragma warning(push)
#pragma warning(disable:4995)
#include <vector>
#pragma warning(pop)
#include <emmintrin.h>
#include <smmintrin.h>

#include <d2d1.h>
#include <dwrite.h>
#include <WinCodec.h>

#pragma comment(lib, "Imm32.lib")
#pragma comment(lib, "Shell32.lib")
#pragma comment(lib, "WindowsCodecs.lib")
#pragma comment(lib, "dwrite.lib")
#pragma comment(lib, "d2d1.lib")

//#import  "interface.tlb" raw_interfaces_only
#include "interface.tlh"
#include "util.h"


namespace Ck{

_COM_SMARTPTR_TYPEDEF(IDropTargetHelper, __uuidof(IDropTargetHelper));

#define WM_USER_DROP_HDROP  WM_USER+10
#define WM_USER_DROP_WSTR   WM_USER+11

class droptarget: public IUnknownImpl2<IDropTarget>{
protected:
	HWND   m_parent;
	DWORD  m_eff;
	DWORD  m_key;
	IDropTargetHelperPtr m_help;
public:
	droptarget(HWND hwnd):m_parent(hwnd),m_eff(DROPEFFECT_NONE), m_key(0){
		m_help.CreateInstance(CLSID_DragDropHelper);
	}
	STDMETHOD(DragOver)(DWORD key, POINTL pt, DWORD* eff){
		*eff = m_eff;
		m_key = key;
		if(m_help) m_help->DragOver((POINT*)&pt, *eff);
		return S_OK;
	}
	STDMETHOD(DragLeave)(void){
		if(m_help) m_help->DragLeave();
		return S_OK;
	}
	STDMETHOD(DragEnter)(IDataObject* data, DWORD key, POINTL pt, DWORD* eff){
		FORMATETC fmt = { CF_HDROP, 0,DVASPECT_CONTENT,-1,TYMED_HGLOBAL};
		HRESULT hr = data->QueryGetData(&fmt);
		if(FAILED(hr)){
			fmt.cfFormat = CF_UNICODETEXT;
			hr = data->QueryGetData(&fmt);
		}
		if(FAILED(hr))
			*eff = DROPEFFECT_NONE;
		m_eff = *eff;
		m_key = key;
		if(m_help) m_help->DragEnter(m_parent, data, (POINT*)&pt, *eff);
		return S_OK;
	}
	STDMETHOD(Drop)(IDataObject* data, DWORD key, POINTL pt, DWORD* eff){
		*eff = m_eff;
		if(m_help) m_help->Drop(data, (POINT*)&pt, *eff);
		if(m_eff != DROPEFFECT_NONE){
			STGMEDIUM stg;
			memset(&stg,0,sizeof(stg));
			FORMATETC fmt = { CF_HDROP, 0,DVASPECT_CONTENT,-1,TYMED_HGLOBAL};
			if(SUCCEEDED(data->GetData(&fmt, &stg))){
				SendMessage(m_parent, WM_USER_DROP_HDROP, (WPARAM)stg.hGlobal, (LPARAM)m_key);
				ReleaseStgMedium(&stg);
			}
			else {
				fmt.cfFormat = CF_UNICODETEXT;
				if(SUCCEEDED(data->GetData(&fmt, &stg))){
					SendMessage(m_parent, WM_USER_DROP_WSTR, (WPARAM)stg.hGlobal, (LPARAM)m_key);
					ReleaseStgMedium(&stg);
				}
			}
		}
		return S_OK;
	}
};



#define SRELEASE(p) if(p){ p->Release(); p=0; }
#define SYSFREE(p)  if(p){ SysFreeString(p); p=0; }
#define HRCHK(func) if(SUCCEEDED(hr)){ hr=func; if(FAILED(hr)){ trace("ERR:0x%08X:%s\n", hr, #func); }}


class Graphics{
private:
	HWND			m_hwnd;
	int			m_width;
	int			m_height;
	bool			m_is_resize;

	IDWriteFactory*		m_dwrite_factory;
	IWICImagingFactory*     m_wic_factory;
	ID2D1Factory*		m_d2d_factory;

	ID2D1HwndRenderTarget*	m_windowRT;
	DWORD			m_solid_color;
	ID2D1SolidColorBrush*	m_solid_brush;

	DWORD			m_background_color;
	DWORD			m_background_place;
	UINT			m_background_width;
	UINT			m_background_height;
	BSTR			m_background_filepath;
	IWICBitmap*		m_background_bitmap;
	ID2D1BitmapBrush*	m_background_brush;

	void  apply_background_place(){
		if(!m_background_brush) return;

		float  bgW = (float) m_background_width;
		float  bgH = (float) m_background_height;
		D2D1_EXTEND_MODE  ex = D2D1_EXTEND_MODE_CLAMP;
		D2D1_EXTEND_MODE  ey = D2D1_EXTEND_MODE_CLAMP;
		float  tx,ty,sx,sy;
		float  zoom = (m_height*bgW/bgH > m_width) ? (m_height/bgH) : (m_width/bgW);

		switch((m_background_place>>24)&0xFF){
		default:
		case Place_NoRepeat: sy = 1.0f; break;
		case Place_Repeat:   sy = 1.0f; ey = D2D1_EXTEND_MODE_WRAP; break;
		case Place_Zoom:     sy = zoom; break;
		case Place_Scale:    sy = m_height/bgH; break;
		}
		switch((m_background_place>>16)&0xFF){
		default:
		case Align_Center:   ty = (m_height - (bgH*sy)) * 0.5f; break;
		case Align_Far:      ty = (m_height - (bgH*sy)); break;
		case Align_Near:     ty = 0.0f; break;
		}

		switch((m_background_place>> 8)&0xFF){
		default:
		case Place_NoRepeat: sx = 1.0f; break;
		case Place_Repeat:   sx = 1.0f; ex = D2D1_EXTEND_MODE_WRAP; break;
		case Place_Zoom:     sx = zoom; break;
		case Place_Scale:    sx = m_width/bgW; break;
		}
		switch((m_background_place>> 0)&0xFF){
		default:
		case Align_Center:   tx = (m_width - (bgW*sx)) * 0.5f; break;
		case Align_Far:      tx = (m_width - (bgW*sx)); break;
		case Align_Near:     tx = 0.0f; break;
		}

		m_background_brush->SetExtendModeX(ex);
		m_background_brush->SetExtendModeY(ey);
		m_background_brush->SetInterpolationMode(D2D1_BITMAP_INTERPOLATION_MODE_LINEAR);
		m_background_brush->SetTransform( D2D1::Matrix3x2F(sx,0.0f, 0.0f,sy, tx,ty) );
	}
	void  create_background_brush(){
		if(m_windowRT && m_background_bitmap && !m_background_brush){
			HRESULT  hr = S_OK;

			ID2D1Bitmap*  bitmap =0;
			HRCHK( m_windowRT->CreateBitmapFromWicBitmap(m_background_bitmap, D2D1::BitmapProperties(D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE), 96.0f, 96.0f), &bitmap) );
			HRCHK( m_windowRT->CreateBitmapBrush(bitmap, &m_background_brush) );
			SRELEASE(bitmap);

			apply_background_place();
		}
	}
	void  set_background_image(BSTR cygpath){
		if(!m_wic_factory) return;

		SRELEASE(m_background_brush);
		SRELEASE(m_background_bitmap);
		SYSFREE (m_background_filepath);

		m_background_filepath = SysAllocString(cygpath);

		//trace("[Image-cyg] \"%S\n", cygpath);

		HRESULT   hr = S_OK;
		BSTR      winpath = Ck::Util::to_windows_path(cygpath);
		if(!winpath) return;

		//trace("[Image-win] \"%S\n", winpath);

		IWICStream*             stream  =0;
		IWICBitmapDecoder*      decoder =0;
		IWICBitmapFrameDecode*  frame   =0;
		IWICFormatConverter*	convert =0;

		HRCHK( m_wic_factory->CreateStream(&stream) );
		HRCHK( stream->InitializeFromFilename(winpath, GENERIC_READ) );
		HRCHK( m_wic_factory->CreateDecoderFromStream(stream, NULL, WICDecodeMetadataCacheOnDemand, &decoder) );
		HRCHK( decoder->GetFrame(0, &frame) );
		HRCHK( m_wic_factory->CreateFormatConverter(&convert) );
		HRCHK( convert->Initialize(frame, GUID_WICPixelFormat32bppBGR, WICBitmapDitherTypeNone, NULL, 0.0f, WICBitmapPaletteTypeCustom) );
		HRCHK( m_wic_factory->CreateBitmapFromSource(convert, WICBitmapCacheOnDemand, &m_background_bitmap) );
		HRCHK( m_background_bitmap->GetSize(&m_background_width, &m_background_height) );

		SRELEASE(convert);
		SRELEASE(frame);
		SRELEASE(decoder);
		SRELEASE(stream);
		SYSFREE(winpath);

		create_background_brush();
	}
	void  free_device(){
		SRELEASE(m_background_brush);
		SRELEASE(m_solid_brush);
		SRELEASE(m_windowRT);
		trace("Graphics::ReleaseRenderTarget\n");
	}
	void  create_device(){
		if(!m_d2d_factory || !m_dwrite_factory) return;

		HRESULT  hr = S_OK;
		D2D1_SIZE_U  size   = D2D1::SizeU( UINT(m_width), UINT(m_height) );

		m_is_resize = true;

		//device
		trace("Graphics::CreateRenderTarget\n");

		HRCHK( m_d2d_factory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED), 96.0f, 96.0f),
			D2D1::HwndRenderTargetProperties(m_hwnd, size, D2D1_PRESENT_OPTIONS_RETAIN_CONTENTS | D2D1_PRESENT_OPTIONS_IMMEDIATELY),
			&m_windowRT) );

		//solid brush
		HRCHK( m_windowRT->CreateSolidColorBrush(colorf(m_solid_color), &m_solid_brush) );

		//bg brush
		create_background_brush();

		//text render params
		IDWriteRenderingParams*  monitor =0;
		IDWriteRenderingParams*  custom =0;
		HRCHK( m_dwrite_factory->CreateMonitorRenderingParams( MonitorFromWindow(m_hwnd, MONITOR_DEFAULTTONEAREST), &monitor ) );
		HRCHK( m_dwrite_factory->CreateCustomRenderingParams(monitor->GetGamma(), monitor->GetEnhancedContrast(), 1.0f, monitor->GetPixelGeometry(), monitor->GetRenderingMode(), &custom) );
		if(SUCCEEDED(hr)){
			//trace_rendering_params("SystemDefault", monitor);
			//trace_rendering_params("Custom", custom);
			m_windowRT->SetTextRenderingParams(custom);
			m_windowRT->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE);
		}
		SRELEASE(monitor);
		SRELEASE(custom);
	}
	static void  trace_rendering_params(LPCSTR title, IDWriteRenderingParams* p){
		LPCSTR  pixelGeom;
		switch(p->GetPixelGeometry()){
		case DWRITE_PIXEL_GEOMETRY_FLAT: pixelGeom = "FLAT"; break;
		case DWRITE_PIXEL_GEOMETRY_RGB:  pixelGeom = "RGB"; break;
		case DWRITE_PIXEL_GEOMETRY_BGR:  pixelGeom = "BGR"; break;
		default:                         pixelGeom = "Unknown"; break;
		}
		LPCSTR  renderMode;
		switch(p->GetRenderingMode()){
		case DWRITE_RENDERING_MODE_DEFAULT:                     renderMode = "Default"; break;
		case DWRITE_RENDERING_MODE_ALIASED:                     renderMode = "Aliased"; break;
		case DWRITE_RENDERING_MODE_CLEARTYPE_GDI_CLASSIC:       renderMode = "ClearType-GDIClassic"; break;
		case DWRITE_RENDERING_MODE_CLEARTYPE_GDI_NATURAL:       renderMode = "ClearType-GDINatural"; break;
		case DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL:           renderMode = "ClearType-Natural"; break;
		case DWRITE_RENDERING_MODE_CLEARTYPE_NATURAL_SYMMETRIC: renderMode = "ClearType-NaturalSymmetric"; break;
		case DWRITE_RENDERING_MODE_OUTLINE:                     renderMode = "Outline"; break;
		default:                                                renderMode = "Unknown"; break;
		}
		trace("TextRenderingParams \"%s\" {\n", title);
		trace("  Gamme            : %f\n", p->GetGamma());
		trace("  EnhancedContrast : %f\n", p->GetEnhancedContrast());
		trace("  ClearTypeLevel   : %f\n", p->GetClearTypeLevel());
		trace("  PixelGeometry    : %s\n", pixelGeom);
		trace("  RenderingMode    : %s\n", renderMode);
		trace("}\n");
	}
	static D2D1_COLOR_F  colorf(DWORD argb){
		D2D1_COLOR_F  rv;
		#if 0
		colorf = D2D1::ColorF((argb>>16)&255/255.0f, (argb>>8)&255/255.0f, (argb>>0)&255/255.0f, (argb>>24)&255/255.0f);
		#elif 0 //SSE4
		__m128i  abgr = _mm_shuffle_epi32(_mm_cvtepu8_epi32(_mm_cvtsi32_si128(argb)), _MM_SHUFFLE(3,0,1,2));
		_mm_storeu_ps( (float*)&rv, _mm_mul_ps(_mm_set1_ps(1.0f/255.0f), _mm_cvtepi32_ps(abgr)));
		#else //SSE2
		const __m128i  zero = _mm_setzero_si128();
		__m128i  abgr = _mm_cvtsi32_si128(argb);
		abgr = _mm_shuffle_epi32(_mm_unpacklo_epi16(_mm_unpacklo_epi8(abgr,zero),zero), _MM_SHUFFLE(3,0,1,2));
		_mm_storeu_ps( (float*)&rv, _mm_mul_ps(_mm_set1_ps(1.0f/255.0f), _mm_cvtepi32_ps(abgr)));
		#endif
		return rv;
	}
	static D2D1_RECT_F  rectf(const RECT& rc){
		D2D1_RECT_F  rv;
		#if 0
		rectf = D2D1::RectF( float(rc.left), float(rc.top), float(rc.right), float(rc.bottom) );
		#else
		_mm_storeu_ps( (float*)&rv, _mm_cvtepi32_ps( _mm_loadu_si128( (__m128i*)&rc )));
		#endif
		return rv;
	}
	static D2D1_RECT_F  rectf_pixelcenter(const RECT& rc){
		D2D1_RECT_F  rv;
		#if 0
		rectf = D2D1::RectF( float(rc.left)+0.5f, float(rc.top)+0.5f, float(rc.right)-0.5f, float(rc.bottom)-0.5f );
		#else
		static const __m128  offset = { +0.5f, +0.5f, -0.5f, -0.5f };
		_mm_storeu_ps( (float*)&rv, _mm_add_ps(offset, _mm_cvtepi32_ps( _mm_loadu_si128( (__m128i*)&rc ))));
		#endif
		return rv;
	}
	void  set_color(DWORD argb){
		if(m_solid_color != argb){
			m_solid_color = argb;
			m_solid_brush->SetColor( colorf(m_solid_color) );
		}
	}
public:
	~Graphics(){
		free_device();
		SRELEASE(m_background_bitmap);
		SYSFREE (m_background_filepath);
		SRELEASE(m_d2d_factory);
		SRELEASE(m_wic_factory);
		SRELEASE(m_dwrite_factory);
	}
	Graphics(HWND hwnd)
		: m_hwnd(hwnd),
		  m_width(0),
		  m_height(0),
		  m_is_resize(false),
		  m_dwrite_factory(0),
		  m_wic_factory(0),
		  m_d2d_factory(0),
		  m_windowRT(0),
		  m_solid_color(0xFF000000),
		  m_solid_brush(0),
		  m_background_color(0x00FFFFFF),
		  m_background_place(0x03010301),
		  m_background_width(0),
		  m_background_height(0),
		  m_background_filepath(0),
		  m_background_bitmap(0),
		  m_background_brush(0){
		HRESULT  hr = S_OK;
		HRCHK( CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, __uuidof(*m_wic_factory), (void**)&m_wic_factory) );
		HRCHK( DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(*m_dwrite_factory), (IUnknown**)&m_dwrite_factory) );
		HRCHK( D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(*m_d2d_factory), (void**)&m_d2d_factory) );
	}

	int   width() const { return m_width;  }
	int   height() const{ return m_height; }

	void  GetDesktopDpi(float* x, float* y) const{
		if(m_d2d_factory){
			m_d2d_factory->GetDesktopDpi(x,y);
		}else{
			*x = *y = 96.0f;
		}
	}

	DWORD GetBackgroundPlace() const{ return m_background_place; }
	BSTR  GetBackgroundFile() const { return m_background_filepath; }

	void  SetBackgroundPlace(DWORD place){
		m_background_place = place;
		apply_background_place();
	}
	void  SetBackgroundFile(BSTR filepath){
		set_background_image(filepath);
	}
	void  SetBackgroundColor(DWORD color){
		m_background_color = color | 0xFF000000;
	}
	void  Resize(int w, int h){
		m_width  = w;
		m_height = h;
		if(m_windowRT){
			HRESULT hr = m_windowRT->Resize(D2D1::SizeU(UINT(m_width), UINT(m_height)));
			if(FAILED(hr)){
				free_device();
			}else{
				m_is_resize = true;
				apply_background_place();
			}
		}
	}
	int  BeginDraw(){
		if(!m_windowRT){
			create_device();
		}
		if(!m_windowRT){
			return 0;
		}

		m_windowRT->BeginDraw();

		if(m_is_resize){
			if(m_background_brush){
				m_windowRT->FillRectangle(D2D1::RectF(0.0f,0.0f,float(m_width),float(m_height)), m_background_brush);
			}else{
				m_windowRT->Clear(colorf(m_background_color));
			}
			return 2;
		}
		return 1;
	}
	void  EndDraw(){
		m_is_resize = false;
		HRESULT  hr = m_windowRT->EndDraw();
		if(hr == D2DERR_RECREATE_TARGET){
			free_device();
		}
	}
	void  PushClipRect(const RECT& rc){
		m_windowRT->PushAxisAlignedClip(rectf(rc), D2D1_ANTIALIAS_MODE_ALIASED);
	}
	void  PopClipRect(){
		m_windowRT->PopAxisAlignedClip();
	}
	void  FillBackground(const RECT& rc, DWORD color){
		const D2D1_RECT_F  rect = rectf(rc);
		const BYTE  alpha = color>>24;
		if(!m_is_resize){
			if(alpha<255){
				if(m_background_brush){
					m_windowRT->FillRectangle(rect, m_background_brush);
				}else{
					m_windowRT->PushAxisAlignedClip(rectf(rc), D2D1_ANTIALIAS_MODE_ALIASED);
					m_windowRT->Clear(colorf(m_background_color));
					m_windowRT->PopAxisAlignedClip();
				}
			}
		}
		if(alpha>0){
			set_color(color);
			m_windowRT->FillRectangle(rect, m_solid_brush);
		}
	}
	void  DrawRect(const RECT& rc, DWORD argb){
		set_color(argb);
		m_windowRT->DrawRectangle( rectf_pixelcenter(rc), m_solid_brush, 1.0f, NULL );
	}
	void  DrawLine(const D2D1_POINT_2F& point0, const D2D1_POINT_2F& point1, DWORD color){
		set_color(color);
		m_windowRT->DrawLine(point0, point1, m_solid_brush, 1.0f, NULL);
	}
	void  DrawGlyphRun(const D2D1_POINT_2F& baselineOrigin, const DWRITE_GLYPH_RUN* glyphRun, DWORD color){
		set_color(color);
		m_windowRT->DrawGlyphRun(baselineOrigin, glyphRun, m_solid_brush);
	}
};


class Font{
private:
	IDWriteFont*		m_font;
	IDWriteFontFace*	m_fontFace;
	float			m_emUnit;
	float			m_ascent;
	float			m_descent;
public:
	~Font(){ Clear(); }
	Font() : m_font(0), m_fontFace(0), m_emUnit(0.0f), m_ascent(0.0), m_descent(0.0f) {}

	void  Clear(){
		SRELEASE(m_fontFace);
		SRELEASE(m_font);
	}
	bool  Init(IDWriteFontFamily* family, bool bold){
		Clear();

		HRESULT  hr = S_OK;

		HRCHK( family->GetFirstMatchingFont( bold ? DWRITE_FONT_WEIGHT_BOLD : DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STRETCH_NORMAL, DWRITE_FONT_STYLE_NORMAL, &m_font) );
		HRCHK( m_font->CreateFontFace(&m_fontFace) );

		DWRITE_FONT_METRICS  metrics;
		m_fontFace->GetMetrics(&metrics);

		if(SUCCEEDED(hr)){
			m_emUnit     = float( metrics.designUnitsPerEm );
			m_ascent     = (metrics.ascent ) / m_emUnit;
			m_descent    = (metrics.descent) / m_emUnit;
			return true;
		}

		Clear();
		return false;
	}

	IDWriteFont*      GetFont() const     { return m_font; }
	IDWriteFontFace*  GetFontFace() const { return m_fontFace; }
	float  GetEmUnit() const              { return m_emUnit;  }
	float  GetAscent() const              { return m_ascent;  }
	float  GetDescent() const             { return m_descent; }
};

class FontFamily{
private:
	IDWriteFontFamily*	m_family;
	Font			m_norm;
	Font			m_bold;
public:
	~FontFamily(){ SRELEASE(m_family); }
	FontFamily() : m_family(0), m_norm(), m_bold() {}

	void  Clear(){
		m_bold.Clear();
		m_norm.Clear();
		SRELEASE(m_family);
	}
	bool  Init(IDWriteFontFamily* family){
		Clear();
		m_family = family;
		m_family->AddRef();
		if(m_norm.Init(m_family, false) &&
		   m_bold.Init(m_family, true) ){
			return true;
		}
		Clear();
		return false;
	}

	const Font&  GetFont(CharFlag style = (CharFlag)0) const{
		return (style & CharFlag_Bold) ? m_bold : m_norm;
	}
};

class FontManager{
private:
	IDWriteFactory*		m_dwriteFactory;
	IDWriteFontCollection*	m_sysFonts;
	UINT32			m_fontCount;
	FontFamily		m_fonts[16];

	float			m_fontWidth;
	float			m_fontHeight;
	float			m_fontBaseline;
	float			m_fontUnderline;

	void  free_fonts(){
		for(UINT32 i=0; i < m_fontCount; ++i){
			m_fonts[i].Clear();
		}
		m_fontCount     = 0;

		m_fontWidth     = 0.0f;
		m_fontHeight    = 0.0f;
		m_fontBaseline  = 0.0f;
		m_fontUnderline = 0.0f;
	}
	void  add_font(LPCWSTR familyName){
		if(m_sysFonts && m_fontCount < _countof(m_fonts) && familyName && familyName[0]){
			HRESULT hr = S_OK;
			UINT32  index =0;
			BOOL    exists = FALSE;
			HRCHK( m_sysFonts->FindFamilyName(familyName, &index, &exists) );
			if(SUCCEEDED(hr)){
				if(exists){
					IDWriteFontFamily*  family =0;
					HRCHK( m_sysFonts->GetFontFamily(index, &family) );
					if(SUCCEEDED(hr)){
						if( m_fonts[m_fontCount].Init(family) ){
							m_fontCount++;
						}
						family->Release();
					}
				}
			}
		}
	}
	IDWriteFontFace*  get_glyph(UINT32 unicode, CharFlag style, UINT16* out_glyph, float* out_glyphWidth) const{
		for(UINT32 k=0; k < m_fontCount; ++k){
			for(;;){
				const Font&       font        = m_fonts[k].GetFont(style);
				IDWriteFont*      dwrFont     = font.GetFont();
				IDWriteFontFace*  dwrFontFace = font.GetFontFace();

				if(!dwrFontFace)
					break;

				BOOL  hasChar;
				if(FAILED( dwrFont->HasCharacter(unicode, &hasChar) ))
					break;
				if(!hasChar)
					break;

				UINT16  glyphIndex;
				if(FAILED( dwrFontFace->GetGlyphIndices(&unicode, 1, &glyphIndex) ))
					break;
				*out_glyph = glyphIndex;

				DWRITE_GLYPH_METRICS   glyphMetrics;
				if(FAILED( dwrFontFace->GetDesignGlyphMetrics(&glyphIndex, 1, &glyphMetrics, FALSE) ))
					break;
				*out_glyphWidth = glyphMetrics.advanceWidth / font.GetEmUnit();

				return dwrFontFace;
			}
		}
		return 0;
	}
	void  init_metrics(){
		if(m_fontCount > 0){
			const Font&  font = m_fonts[0].GetFont();
			IDWriteFontFace*  dwrFontFace = font.GetFontFace();
			if(dwrFontFace){
				HRESULT  hr = S_OK;
				float    width = 0.0f;

				const char*   text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				const int     textLen = sizeof(text)-1;

				UINT32   unicodes     [textLen];
				UINT16   glyphIndices [textLen];
				for(int i=0; i < textLen; ++i){
					unicodes[i] = (UINT32) text[i];
				}

				HRCHK( dwrFontFace->GetGlyphIndices(unicodes, textLen, glyphIndices) );

				DWRITE_GLYPH_METRICS  glyphMetrics [textLen];
				HRCHK( dwrFontFace->GetDesignGlyphMetrics(glyphIndices, textLen, glyphMetrics, FALSE) );

				for(int i=0; i < textLen; ++i)
					width += glyphMetrics[i].advanceWidth;
				width /= float(textLen);

				DWRITE_FONT_METRICS  fontMetrics;
				dwrFontFace->GetMetrics(&fontMetrics);

				#if 0
				trace("fontMetrics{\n");
				trace("  designUnitsPerEm = %u\n", fontMetrics.designUnitsPerEm);
				trace("  averageWidth = %f\n", width);
				trace("  ascent    = %u\n", fontMetrics.ascent);
				trace("  descent   = %u\n", fontMetrics.descent);
				trace("  lineGap   = %d\n", fontMetrics.lineGap);
				trace("  capHeight = %u\n", fontMetrics.capHeight);
				trace("  xHeight   = %u\n", fontMetrics.xHeight);
				trace("  underlinePosition  = %d\n", fontMetrics.underlinePosition);
				trace("  underlineThickness = %u\n", fontMetrics.underlineThickness);
				trace("  strikethroughPosition  = %d\n", fontMetrics.strikethroughPosition);
				trace("  strikethroughThickness = %u\n", fontMetrics.strikethroughThickness);
				trace("}\n");
				#endif

				const float  emUnit = (float) fontMetrics.designUnitsPerEm;
				m_fontWidth     = (width)                                    / emUnit;
				m_fontHeight    = (fontMetrics.ascent + fontMetrics.descent) / emUnit;
				m_fontBaseline  = (fontMetrics.ascent)                       / emUnit;
				m_fontUnderline = (-fontMetrics.underlinePosition)           / emUnit;
			}
		}
	}
public:
	~FontManager(){
		free_fonts();
	}
	FontManager() : m_dwriteFactory(0), m_sysFonts(0), m_fontCount(0), m_fontWidth(0.0f), m_fontHeight(0.0f), m_fontBaseline(0.0f), m_fontUnderline(0.0f){
		HRESULT  hr = S_OK;
		HRCHK( DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(*m_dwriteFactory), (IUnknown**)&m_dwriteFactory) );
		HRCHK( m_dwriteFactory->GetSystemFontCollection(&m_sysFonts, FALSE) );
	}
	void  SetFont(LPCWSTR familyName){
		free_fonts();
		add_font(familyName);
		add_font(L"Consolas");
		add_font(L"Lucida Console");
		add_font(L"Meiryo");
		add_font(L"MS Gothic");
		init_metrics();
	}
	float  GetFontWidth() const{
		return m_fontWidth;
	}
	float  GetFontHeight() const{
		return m_fontHeight;
	}
	float  GetBaseline() const{
		return m_fontBaseline;
	}
	float  GetUnderline() const{
		return m_fontUnderline;
	}
	void  GetLOGFONT(LOGFONTW* output) const{
		if(m_fontCount > 0){
			const Font&  font = m_fonts[0].GetFont();
			IDWriteFont*  dwrFont = font.GetFont();
			if(dwrFont){
				HRESULT  hr = S_OK;
				IDWriteGdiInterop*  gdi =0;
				BOOL  sys;
				HRCHK( m_dwriteFactory->GetGdiInterop(&gdi) );
				HRCHK( gdi->ConvertFontToLOGFONT(dwrFont, output, &sys) );
				SRELEASE(gdi);
				if(SUCCEEDED(hr)){
					return;
				}
			}
		}
		memset(output, 0, sizeof(*output));
	}
	IDWriteFontFace*  GetGlyph(UINT32 unicode, CharFlag style, UINT16* out_glyph, float* out_glyphWidth){
		IDWriteFontFace* rv = get_glyph(unicode, style, out_glyph, out_glyphWidth);
		#if 1
		if(!rv){
			if(m_sysFonts && m_fontCount < _countof(m_fonts)){
				UINT32  count = m_sysFonts->GetFontFamilyCount();
				bool    add = false;
				for(UINT32 k=0; k < count; ++k){
					IDWriteFontFamily* family;
					if(SUCCEEDED( m_sysFonts->GetFontFamily(k, &family) )){
						IDWriteFont* font;
						if(SUCCEEDED( family->GetFirstMatchingFont(DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STRETCH_NORMAL, DWRITE_FONT_STYLE_NORMAL, &font) )){
							BOOL  hasChar;
							if(SUCCEEDED( font->HasCharacter(unicode, &hasChar) )){
								if(hasChar){
									if( m_fonts[m_fontCount].Init(family) ){
										m_fontCount++;
										add = true;
									}
								}
							}
							font->Release();
						}
						family->Release();
					}
					if(add) break;
				}
				if(add){
					rv = get_glyph(unicode, style, out_glyph, out_glyphWidth);
				}
			}
		}
		#endif
		return rv;
	}
};



inline DWORD  composite_argb(DWORD dst, const DWORD src){
	UINT  da, sa, sf;

	da = dst>>24;
	sa = src>>24;

	da = (da * (255-sa)) / 255;
	da = da + sa;
	sf = (da) ? ((sa<<16)/da) : 0;

	DWORD  rv;
	BYTE*  O = (BYTE*)(&rv);
	const BYTE* D = (const BYTE*)(&dst);
	const BYTE* S = (const BYTE*)(&src);
	O[0] = (BYTE) (( (S[0]-D[0]) * sf) >> 16) + D[0];
	O[1] = (BYTE) (( (S[1]-D[1]) * sf) >> 16) + D[1];
	O[2] = (BYTE) (( (S[2]-D[2]) * sf) >> 16) + D[2];
	O[3] = (BYTE) da;
	return rv;
}

static SIZE get_window_frame_size(HWND hwnd){
	WINDOWINFO wi;
	wi.cbSize = sizeof(wi);
	GetWindowInfo(hwnd, &wi);

	RECT rc = {0,0,0,0};
	AdjustWindowRectEx(&rc, wi.dwStyle, FALSE, wi.dwExStyle);

	if(wi.dwStyle & WS_VSCROLL){
		rc.right += GetSystemMetrics(SM_CXVSCROLL);
	}

	SIZE sz = { rc.right-rc.left, rc.bottom-rc.top };
	return sz;
}


//----------------------------------------------------------------------------

class IWindowNotify_{
public:
	STDMETHOD(OnClosed)() = 0;
	STDMETHOD(OnKeyDown)(DWORD vk) = 0;
	STDMETHOD(OnMouseWheel)(int delta) = 0;
	STDMETHOD(OnMenuInit)(HMENU menu) = 0;
	STDMETHOD(OnMenuExec)(DWORD id) = 0;
	STDMETHOD(OnTitleInit)() = 0;
	STDMETHOD(OnDrop)(BSTR bs, int type, DWORD key) = 0;
	STDMETHOD(OnPasteClipboard)() = 0;
};

//----------------------------------------------------------------------------

class Window_{
	static const int TIMERID_SCREEN = 0x673;
	static const int TIMERID_MOUSE  = 0x674;
	inline void set_screen_timer()  {  SetTimer(m_hwnd, TIMERID_SCREEN, 16, 0);}
	inline void kill_screen_timer() { KillTimer(m_hwnd, TIMERID_SCREEN);}
	inline void set_mouse_timer()   {  SetTimer(m_hwnd, TIMERID_MOUSE, 20, 0);}
	inline void kill_mouse_timer()  { KillTimer(m_hwnd, TIMERID_MOUSE);}

	enum CID{
		CID_FG = 256,
		CID_BG,
		CID_Select,
		CID_Cursor,
		CID_ImeCursor,
		CID_Max,
	};

	IPty*		m_pty;
	IWindowNotify_* const  m_notify;
	HWND		m_hwnd;
	int		m_win_posx;
	int		m_win_posy;
	int		m_char_width;
	int		m_char_height;
	Graphics*	m_graphics;
	FontManager	m_fontManager;
	Snapshot*	m_snapshot;
	int		m_screen_tick;
	float		m_font_size;
	int		m_font_width;
	int		m_font_height;
	LOGFONT 	m_font_log;
	MouseCmd	m_lbtn_cmd;
	MouseCmd	m_mbtn_cmd;
	MouseCmd	m_rbtn_cmd;
	WinTransp	m_transp_mode;
	WinZOrder	m_zorder;
	RECT		m_border;
	int		m_linespace;
	int		m_vscrl_mode;
	SCROLLINFO	m_vscrl;
	int		m_wheel_delta;
	VARIANT_BOOL	m_blink_cursor;
	BYTE		m_cursor_step;
	bool		m_is_active;
	VARIANT_BOOL	m_ime_on;
	//
	DWORD		m_colors[CID_Max];
	BYTE		m_bg_alpha;
	//
	bool		m_click;
	bool		m_click_moved;
	BYTE		m_click_count;
	MouseCmd	m_click_cmd;
	DWORD		m_click_time;
	int		m_click_posx;
	int		m_click_posy;
	int		m_click_scroll;
	int		m_click_dx;
	int		m_click_dy;
	//
	std::vector<UINT32>			m_unicode_text;
	std::vector<float>			m_unicode_step;
	std::vector<UINT16>			m_unicode_glyphindices;
	std::vector<DWRITE_GLYPH_OFFSET>	m_unicode_glyphoffsets;
protected:
	void draw_text(RECT& rect, CharFlag style, UINT32* unicodeText, FLOAT* unicodeStep, UINT32 unicodeLen);
	void draw_screen();

	HRESULT _setup_font(LPCWSTR familyName, float fontSize){

		if(familyName){
			StringCbCopy(m_font_log.lfFaceName, sizeof(m_font_log.lfFaceName), familyName);
			m_fontManager.SetFont( familyName );
		}

		if(fontSize < 8.0f)  fontSize = 8.0f;
		if(fontSize > 72.0f) fontSize = 72.0f;

		float  em2pixelX, em2pixelY;
		m_graphics->GetDesktopDpi(&em2pixelX, &em2pixelY);
		em2pixelX = (em2pixelX/72.0f) * fontSize;
		em2pixelY = (em2pixelY/72.0f) * fontSize;

		m_font_size   = fontSize;
		m_font_width  = (int) std::floor(m_fontManager.GetFontWidth () * em2pixelX + 0.9f);
		m_font_height = (int) std::floor(m_fontManager.GetFontHeight() * em2pixelY + 0.9f);

		#if 0
		trace("FontSize   = %fpt\n",  m_font_size);
		trace("FontWidth  = %dpx\n",  m_font_width);
		trace("FontHeight = %dpx\n",  m_font_height);
		#endif

		m_fontManager.GetLOGFONT(&m_font_log);
		m_font_log.lfHeight = (LONG) -std::floor(em2pixelY +0.9f);

		Resize(m_char_width, m_char_height);

		HIMC imc = ImmGetContext(m_hwnd);
		if(imc){
			ImmSetCompositionFont(imc, &m_font_log);
			ImmReleaseContext(m_hwnd,imc);
		}
		return S_OK;
	}
	void popup_menu(bool curpos){
		POINT pt;
		if(curpos){
			GetCursorPos(&pt);
		}
		else{
			pt.x=0;
			pt.y=-1;
			ClientToScreen(m_hwnd,&pt);
		}
		HMENU menu = CreatePopupMenu();
		m_notify->OnMenuInit((HMENU)menu);
		//
		DWORD id = (DWORD) TrackPopupMenuEx(menu, TPM_LEFTALIGN|TPM_TOPALIGN|TPM_LEFTBUTTON|TPM_RETURNCMD, pt.x, pt.y, m_hwnd, 0);
		DestroyMenu(menu);
		//
		m_notify->OnMenuExec(id);
	}

	void ime_set_state(){
		HIMC imc = ImmGetContext(m_hwnd);
		if(imc){
			ImmSetOpenStatus(imc, m_ime_on);
			ImmSetCompositionFont(imc, &m_font_log);
			ImmReleaseContext(m_hwnd, imc);
		}
	}
	void ime_set_position(){
		if(m_pty){
			HIMC imc = ImmGetContext(m_hwnd);
			if(imc){
				int x,y;
				m_pty->get_CursorPosX(&x);
				m_pty->get_CursorPosY(&y);
				x = m_border.left + 0 + (x * (m_font_width + 0));
				y = m_border.top  + (m_linespace>>1) + (y * (m_font_height + m_linespace));
				COMPOSITIONFORM cf;
				cf.dwStyle = CFS_POINT;
				cf.ptCurrentPos.x = x;
				cf.ptCurrentPos.y = y;
				ImmSetCompositionWindow(imc,&cf);
				ImmReleaseContext(m_hwnd,imc);
			}
		}
	}
	void ime_send_result_string(){
		if(m_pty){
			HIMC imc = ImmGetContext(m_hwnd);
			if(imc){
				LONG n = ImmGetCompositionString(imc, GCS_RESULTSTR, 0, 0);
				if(n>0){
					BSTR bs = SysAllocStringByteLen(0, n+sizeof(WCHAR));
					if(bs){
						n = ImmGetCompositionString(imc, GCS_RESULTSTR, bs, n);
						if(n>0){
							bs[n/sizeof(WCHAR)] = '\0';
							((UINT*)bs)[-1] = n;
							m_pty->PutString(bs);
						}
						SysFreeString(bs);
					}
				}
				ImmReleaseContext(m_hwnd,imc);
			}
		}
	}
	//
	static LRESULT CALLBACK wndproc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp){
		Window_* p;
		if(msg==WM_CREATE){
			p = (Window_*) ((CREATESTRUCT*)lp)->lpCreateParams;
			SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)p);
		}
		else{
			p = (Window_*) GetWindowLongPtr(hwnd, GWLP_USERDATA);
		}
		return (p)? p->wm_on_message(hwnd,msg,wp,lp): DefWindowProc(hwnd,msg,wp,lp);
	}
	LRESULT wm_on_message(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp){
		switch(msg){
		case WM_NCDESTROY:
			kill_mouse_timer();
			kill_screen_timer();
			if(m_graphics){
				delete m_graphics;
				m_graphics = 0;
			}
			RevokeDragDrop(m_hwnd);
			DefWindowProc(hwnd,msg,wp,lp);
			trace("Window_::wm_on_nc_destroy\n");
			m_hwnd = 0;
			m_notify->OnClosed();
			return 0;
		case WM_CREATE:
			m_hwnd = hwnd;
			wm_on_create();
			break;
		case WM_ACTIVATE:
			if(LOWORD(wp) == WA_INACTIVE){
				m_is_active = false;
				m_cursor_step = 0;
				m_wheel_delta = 0;
				draw_screen();
			}
			else{//active
				m_is_active = true;
				m_cursor_step = 0;
				draw_screen();
				ime_set_state();
				if(m_pty){
					m_pty->Resize(m_char_width, m_char_height);
				}
			}
			break;
		case WM_GETMINMAXINFO:     wm_on_get_min_max_info((MINMAXINFO*)lp);return 0;
		case WM_WINDOWPOSCHANGING: wm_on_window_pos_changing((WINDOWPOS*)lp);return 0;
		case WM_WINDOWPOSCHANGED:  wm_on_window_pos_changed ((WINDOWPOS*)lp);return 0;
		case WM_TIMER:       wm_on_timer((DWORD)wp);break;
		case WM_MOUSEMOVE:   wm_on_mouse_move(GET_X_LPARAM(lp), GET_Y_LPARAM(lp));return 0;
		case WM_LBUTTONDOWN: wm_on_mouse_down(GET_X_LPARAM(lp), GET_Y_LPARAM(lp), m_lbtn_cmd);return 0;
		case WM_LBUTTONUP:   wm_on_mouse_up  (GET_X_LPARAM(lp), GET_Y_LPARAM(lp), m_lbtn_cmd);return 0;
		case WM_MBUTTONDOWN: wm_on_mouse_down(GET_X_LPARAM(lp), GET_Y_LPARAM(lp), m_mbtn_cmd);return 0;
		case WM_MBUTTONUP:   wm_on_mouse_up  (GET_X_LPARAM(lp), GET_Y_LPARAM(lp), m_mbtn_cmd);return 0;
		case WM_RBUTTONDOWN: wm_on_mouse_down(GET_X_LPARAM(lp), GET_Y_LPARAM(lp), m_rbtn_cmd);return 0;
		case WM_RBUTTONUP:   wm_on_mouse_up  (GET_X_LPARAM(lp), GET_Y_LPARAM(lp), m_rbtn_cmd);return 0;

		case WM_ERASEBKGND:
			return 1;
		case WM_PAINT:
			wm_on_paint();
			return 0;
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
			m_cursor_step = 0;
			m_notify->OnKeyDown((DWORD)wp);
			return 0;
		case WM_MOUSEWHEEL:
			{
				m_wheel_delta += GET_WHEEL_DELTA_WPARAM(wp);
				int notch = m_wheel_delta / WHEEL_DELTA;
				if(notch){
					m_wheel_delta -= notch * WHEEL_DELTA;
					m_notify->OnMouseWheel(notch);
				}
			}
			return 0;
		case WM_INITMENUPOPUP:
			if(HIWORD(lp)){
				UINT nb=GetMenuItemCount((HMENU)wp);
				for(UINT i=0; i<nb; i++){
					if(GetMenuItemID((HMENU)wp, i)==SC_CLOSE){
						for(UINT pos=(i+=2); i<nb; i++)
							DeleteMenu((HMENU)wp, pos, MF_BYPOSITION);
						break;
					}
				}
				m_notify->OnMenuInit((HMENU)wp);
			}
			break;
		case WM_SYSCOMMAND:
			if((wp & 0xFFF0) < 0xF000){
				m_notify->OnMenuExec((DWORD)wp);
				return 0;
			}
			break;
		case WM_CONTEXTMENU:
			if(GET_X_LPARAM(lp)==-1 && GET_Y_LPARAM(lp)==-1){
				popup_menu(false);
				return 0;
			}
			break;
		case WM_VSCROLL:
			wm_on_vscroll(LOWORD(wp));
			break;
		case WM_CHAR:
		case WM_SYSCHAR:
		case WM_IME_CHAR:
			return 0;
		case WM_IME_NOTIFY:
			if(wp == IMN_SETOPENSTATUS){
				if(m_is_active){
					HIMC imc = ImmGetContext(m_hwnd);
					if(imc){
						m_ime_on = ImmGetOpenStatus(imc)? TRUE: FALSE;
						ImmReleaseContext(m_hwnd, imc);
						draw_screen();
					}
				}
			}
			break;
		case WM_IME_STARTCOMPOSITION:
			ime_set_position();
			break;
		case WM_IME_COMPOSITION:
			if(lp & GCS_CURSORPOS)
				ime_set_position();
			if(lp & GCS_RESULTSTR)
				ime_send_result_string();
			break;
		case WM_DWMCOMPOSITIONCHANGED:
			Util::set_window_transp(m_hwnd, m_transp_mode);
			break;
		case WM_USER_DROP_HDROP:
			if(wm_on_user_drop_hdrop((HGLOBAL)wp,(BOOL)lp))
				return 0;
			break;
		case WM_USER_DROP_WSTR:
			if(wm_on_user_drop_wstr((HGLOBAL)wp,(BOOL)lp))
				return 0;
			break;
		}
		return DefWindowProc(hwnd,msg,wp,lp);
	}
	void wm_on_create(){
		HMENU menu = GetSystemMenu(m_hwnd,FALSE);
		if(menu){
			MENUITEMINFO mi;
			mi.cbSize = sizeof(mi);
			mi.fType = MFT_SEPARATOR;
			mi.fMask = MIIM_FTYPE;
			InsertMenuItem(menu, GetMenuItemCount(menu), TRUE, &mi);
		}
		if(m_vscrl_mode & 1){
			SetScrollInfo(m_hwnd, SB_VERT, &m_vscrl, TRUE);
		}

		m_graphics = new Graphics(m_hwnd);
		_setup_font(L"", 10.0f);

		IDropTarget* p = new droptarget(m_hwnd);
		p->AddRef();
		RegisterDragDrop(m_hwnd, p);
		p->Release();

		set_screen_timer();
	}

	void wm_on_get_min_max_info(MINMAXINFO* p){
		SIZE frame = get_window_frame_size(m_hwnd);
		p->ptMinTrackSize.x = frame.cx + (m_border.left + m_border.right) + (m_font_width + 0);
		p->ptMinTrackSize.y = frame.cy + (m_border.top + m_border.bottom) + (m_font_height + m_linespace);
	}
	void wm_on_window_pos_changing(WINDOWPOS* p){
		if(m_zorder == WinZOrder_Bottom){
			if(!(p->flags & SWP_NOZORDER))
				p->hwndInsertAfter = HWND_BOTTOM;
		}
		if(!(p->flags & SWP_NOSIZE) && !IsZoomed(m_hwnd)){
			SIZE frame = get_window_frame_size(m_hwnd);
			int chaW = (p->cx - frame.cx - (m_border.left + m_border.right)) / (m_font_width  + 0);
			int chaH = (p->cy - frame.cy - (m_border.top + m_border.bottom)) / (m_font_height + m_linespace);
			int cliW = (m_border.left + m_border.right) + (chaW * (m_font_width  + 0));
			int cliH = (m_border.top + m_border.bottom) + (chaH * (m_font_height + m_linespace));
			int winW = (cliW + frame.cx);
			int winH = (cliH + frame.cy);
			if(!(p->flags & SWP_NOMOVE)){
				if(p->x != m_win_posx) p->x += (p->cx - winW);
				if(p->y != m_win_posy) p->y += (p->cy - winH);
			}
			p->cx = winW;
			p->cy = winH;
		}
	}
	void wm_on_window_pos_changed(WINDOWPOS* p){
		if(!(p->flags & SWP_NOSIZE)){
			SIZE frame = get_window_frame_size(m_hwnd);
			int chaW = (p->cx - frame.cx - (m_border.left + m_border.right)) / (m_font_width  + 0);
			int chaH = (p->cy - frame.cy - (m_border.top + m_border.bottom)) / (m_font_height + m_linespace);
			int cliW,cliH;
			if(IsZoomed(m_hwnd)){
				cliW = p->cx - frame.cx;
				cliH = p->cy - frame.cy;
			}
			else{
				cliW = (m_border.left + m_border.right) + (chaW * (m_font_width  + 0));
				cliH = (m_border.top + m_border.bottom) + (chaH * (m_font_height + m_linespace));
				int winW = (cliW + frame.cx);
				int winH = (cliH + frame.cy);
				if(winW != p->cx || winH != p->cy){
					if(!(p->flags & SWP_NOMOVE)){
						if(p->x != m_win_posx) p->x += (p->cx - winW);
						if(p->y != m_win_posy) p->y += (p->cy - winH);
						SetWindowPos(m_hwnd,0, p->x,p->y,winW,winH, SWP_NOZORDER|SWP_NOACTIVATE);
					}
					else{
						SetWindowPos(m_hwnd,0, 0,0,winW,winH, SWP_NOMOVE|SWP_NOZORDER|SWP_NOACTIVATE);
					}
					return;
				}
			}
			if(m_char_width != chaW || m_char_height != chaH){
				m_char_width  = chaW;
				m_char_height = chaH;
				if(m_pty){
					m_pty->Resize(chaW, chaH);
				}
			}
			if(m_graphics->width() != cliW || m_graphics->height() != cliH){
				m_graphics->Resize(cliW, cliH);
				draw_screen();
			}
		}
		if(!(p->flags & SWP_NOMOVE)){
			m_win_posx = p->x;
			m_win_posy = p->y;
		}
	}

	void wm_on_paint(){
		ValidateRect(m_hwnd, NULL);
	}

	void wm_on_vscroll(int req){
		if(m_pty){
			int n = 0;
			switch(req){
			case SB_BOTTOM:   n=-1;break;
			case SB_TOP:      n=0;break;
			case SB_PAGEUP:   n=m_vscrl.nPos-m_vscrl.nPage; if(n<0)n=0; break;
			case SB_PAGEDOWN: n=m_vscrl.nPos+m_vscrl.nPage;break;
			case SB_LINEUP:   n=m_vscrl.nPos-1; if(n<0)n=0;break;
			case SB_LINEDOWN: n=m_vscrl.nPos+1;break;
			case SB_THUMBTRACK:
				{
					SCROLLINFO si;
					si.cbSize = sizeof(si);
					si.fMask = SIF_TRACKPOS;
					GetScrollInfo(m_hwnd, SB_VERT, &si);
					n = si.nTrackPos;
				}
				break;
			default:
				return;
			}
			m_pty->put_ViewPos(n);
		}
	}

	void on_select_move(int x, int y, bool start){
		if(m_pty){
			//int  i;
			//m_pty->get_ViewPos(&i);
			m_pty->SetSelection(x, y, (m_click_count & 3) | (start ? 4 : 0));
		}
	}
	void wm_on_mouse_down(int x, int y, MouseCmd cmd){
		DWORD now = GetTickCount();
		DWORD st = (now>m_click_time) ? (now-m_click_time) : (now + ~m_click_time +1);
		int   sx = (x>m_click_posx)? (x-m_click_posx) : (m_click_posx-x);
		int   sy = (y>m_click_posy)? (y-m_click_posy) : (m_click_posy-y);
		if(cmd == m_click_cmd &&
		   st <= GetDoubleClickTime() &&
		   sx <= GetSystemMetrics(SM_CXDOUBLECLK) &&
		   sy <= GetSystemMetrics(SM_CYDOUBLECLK))
			m_click_count++;
		else
			m_click_count=0;
		m_click_time = now;
		m_click_cmd = cmd;
		m_click = true;
		m_click_moved = false;
		m_click_posx = x;
		m_click_posy = y;
		SetCapture(m_hwnd);
		m_click_dx = x = (x - m_border.left) / (m_font_width + 0);
		m_click_dy = y = (y - m_border.top ) / (m_font_height + m_linespace);
		if(m_click_cmd == MouseCmd_Select){
			on_select_move(x, y, true);
			set_mouse_timer();
		}
		else if(m_click_cmd == MouseCmd_Paste){
			m_click_scroll = y;
			set_mouse_timer();
		}
		else if(m_click_cmd == MouseCmd_Menu){
		}
	}
	void wm_on_timer(DWORD id){
		if(id == TIMERID_SCREEN){
			draw_screen();
		}
		else if(id == TIMERID_MOUSE){
			if(m_click && m_pty){
				if(m_click_cmd == MouseCmd_Select){
					//trace("timer %d %d\n", m_select_x2,m_select_y2);
					int n;
					if(m_click_dy < 0){
						n = (m_click_dy);
						if(n<-10) n=-10;
					}
					else if(m_click_dy >= m_char_height){
						n = (m_click_dy - m_char_height) +1;
						if(n>10) n=10;
					}
					else{
						return;
					}
					int i;
					m_pty->get_ViewPos(&i);
					n += i;
					if(n<0)n=0;
					if(n != i){
						m_pty->put_ViewPos(n);
						m_pty->get_ViewPos(&n);
						if(n != i){
							on_select_move(m_click_dx, m_click_dy, false);
						}
					}
				}
				else if(m_click_cmd == MouseCmd_Paste){
					int n = (m_click_dy - m_click_scroll) / 2;
					if(n<-10) n=-10; else if(n>10) n=10;
					int i;
					m_pty->get_ViewPos(&i);
					n += i;
					if(n<0)n=0;
					if(n != i){
						m_pty->put_ViewPos(n);
						m_pty->get_ViewPos(&n);
					}
				}
			}
		}
	}
	void wm_on_mouse_move(int x, int y){
		if(m_click){
			x = (x - m_border.left) / (m_font_width + 0);
			y = (y - m_border.top ) / (m_font_height + m_linespace);
			if(m_click_dx != x || m_click_dy != y){
				m_click_moved = true;
				m_click_dx = x;
				m_click_dy = y;
				if(m_click_cmd == MouseCmd_Select){
					on_select_move(x,y,false);
				}
				else if(m_click_cmd == MouseCmd_Paste){
				}
				else if(m_click_cmd == MouseCmd_Menu){
				}
			}
		}
	}
	void wm_on_mouse_up(int x, int y, MouseCmd cmd){
		if(m_click && cmd == m_click_cmd){
			m_click = false;
			ReleaseCapture();
			x = (x - m_border.left) / (m_font_width + 0);
			y = (y - m_border.top ) / (m_font_height + m_linespace);
			if(m_click_cmd == MouseCmd_Select){
				kill_mouse_timer();
				if(m_click_dx != x || m_click_dy != y)
					on_select_move(x,y,false);
				if(m_pty){
					BSTR bs;
					m_pty->get_SelectedString(&bs);
					if(bs){
						Util::set_clipboard_text(bs);
						SysFreeString(bs);
					}
				}
			}
			else if(m_click_cmd == MouseCmd_Paste){
				kill_mouse_timer();
				if(!m_click_moved){
					m_notify->OnPasteClipboard();
				}
			}
			else if(m_click_cmd == MouseCmd_Menu){
				popup_menu(true);
			}
		}
	}

	bool wm_on_user_drop_hdrop(HGLOBAL mem, DWORD key){
		if(mem){
			HDROP drop = (HDROP) GlobalLock(mem);
			if(drop){
				BSTR  bs = SysAllocStringLen(0, MAX_PATH+32);
				if(bs){
					DWORD nb = DragQueryFile(drop, (DWORD)-1, 0,0);
					for(DWORD i=0; i<nb; i++){
						int len = (int) DragQueryFile(drop, i, bs, MAX_PATH+8);
						if(len>0){
							bs[len] = '\0';
							((UINT*)bs)[-1] = len * sizeof(WCHAR);
							m_notify->OnDrop(bs, 1, key);
						}
					}
					SysFreeString(bs);
				}
				GlobalUnlock(mem);
				return true;
			}
		}
		return false;
	}
	bool wm_on_user_drop_wstr(HGLOBAL mem, DWORD key){
		if(mem){
			LPWSTR wstr = (LPWSTR) GlobalLock(mem);
			if(wstr){
				BSTR bs = SysAllocString(wstr);
				if(bs){
					m_notify->OnDrop(bs, 0, key);
				}
				GlobalUnlock(mem);
				return true;
			}
		}
		return false;
	}

	//---

	void _finalize(){
		trace("Window_::dtor\n");
		if(m_pty){
			if(m_snapshot){
				m_pty->_del_snapshot(m_snapshot);
				m_snapshot = 0;
			}
			m_pty->Release();
			m_pty = 0;
		}
		if(m_hwnd){
			DestroyWindow(m_hwnd);
		}
	}
public:
	~Window_(){ _finalize();}
	Window_(IWindowNotify_* cb)
		: m_pty(0),
		  m_notify(cb),
		  m_hwnd(0),
		  m_win_posx(0),
		  m_win_posy(0),
		  m_char_width(80),
		  m_char_height(24),
		  m_graphics(0),
		  m_fontManager(),
		  m_snapshot(0),
		  m_screen_tick(0),
		  m_font_size(10.0f),
		  m_font_width(0),
		  m_font_height(0),
		  m_lbtn_cmd(MouseCmd_Select),
		  m_mbtn_cmd(MouseCmd_Paste),
		  m_rbtn_cmd(MouseCmd_Menu),
		  m_transp_mode(WinTransp_None),
		  m_zorder(WinZOrder_Normal),
		  m_linespace(0),
		  m_vscrl_mode(1),
		  m_wheel_delta(0),
		  m_blink_cursor(TRUE),
		  m_cursor_step(0),
		  m_is_active(false),
		  m_ime_on(FALSE),
		  m_bg_alpha(0xCC),
		  m_click(false),
		  m_click_moved(false),
		  m_click_count(0),
		  m_click_cmd(MouseCmd_None),
		  m_click_time(0),
		  m_click_posx(0),
		  m_click_posy(0),
		  m_click_scroll(0),
		  m_click_dx(0),
		  m_click_dy(0),
		  m_unicode_text(256),
		  m_unicode_step(256),
		  m_unicode_glyphindices(256),
		  m_unicode_glyphoffsets(256){
		trace("Window_::ctor\n");
		memset(&m_font_log, 0, sizeof(m_font_log));
		m_font_log.lfCharSet        = DEFAULT_CHARSET;
		m_font_log.lfOutPrecision   = OUT_DEFAULT_PRECIS;
		m_font_log.lfClipPrecision  = CLIP_DEFAULT_PRECIS;
		m_font_log.lfQuality        = DEFAULT_QUALITY;
		m_font_log.lfPitchAndFamily = FIXED_PITCH | FF_DONTCARE;
		memset(&m_border, 0, sizeof(m_border));
		memset(&m_vscrl, 0, sizeof(m_vscrl));
		m_vscrl.cbSize = sizeof(m_vscrl);
		m_vscrl.fMask = SIF_DISABLENOSCROLL | SIF_PAGE | SIF_POS | SIF_RANGE;
		memset(&m_colors, 0, sizeof(m_colors));
		try{
			static const BYTE  table[]={0x00,0x5F,0x87,0xAF,0xD7,0xFE};
			static const DWORD color16[]={
				0x000000,0xCD0000,0x00CD00,0xCDCD00, 0x0000CD,0xCD00CD,0x00CDCD,0xE5E5E5,
				0x4D4D4D,0xFF0000,0x00FF00,0xFFFF00, 0x0000FF,0xFF00FF,0x00FFFF,0xFFFFFF,
			};
			int r,g,b, i=0;
			for(g=0; g<16; g++, i++){
				m_colors[i] = color16[g];
			}
			for(r=0; r<6; r++){
				for(g=0; g<6; g++){
					for(b=0; b<6; b++, i++){
						m_colors[i] = (table[r]<<16) | (table[g]<<8) | (table[b]);
					}
				}
			}
			for(g=8; g<248; g+=10,i++){
				m_colors[i] = (g<<16) | (g<<8) | (g);
			}
			m_colors[CID_FG]        = 0xFFFFFF;
			m_colors[CID_BG]        = 0x000000;
			m_colors[CID_Select]    = 0x663297FD;
			m_colors[CID_Cursor]    = 0x00AA00;
			m_colors[CID_ImeCursor] = 0xAA0000;

			static bool regist = false;
			LPCWSTR classname = L"ckWindowClass";
			if(!regist){
				regist = true;
				WNDCLASSEX wc;
				memset(&wc, 0, sizeof(wc));
				wc.cbSize = sizeof(wc);
				wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
				wc.lpfnWndProc = wndproc;
				wc.hInstance = g_this_module;
				wc.hCursor = LoadCursor(0, IDC_ARROW);
				wc.hIcon = wc.hIconSm = LoadIcon(g_this_module, MAKEINTRESOURCE(1));
				wc.lpszClassName = classname;
				if(!RegisterClassEx(&wc))
					throw (HRESULT) E_FAIL;
			}
			HWND hwnd = CreateWindowEx(0, classname, 0, WS_OVERLAPPEDWINDOW | WS_VSCROLL,
				CW_USEDEFAULT,CW_USEDEFAULT,0,0, 0,0,g_this_module,this);
			if(!hwnd) throw (HRESULT) E_FAIL;
		}
		catch(...){
			_finalize();
			throw;
		}
	}

	STDMETHOD(get_Handle)(UINT* p){
		*p = (UINT) m_hwnd;
		return S_OK;
	}
	STDMETHOD(get_Pty)(VARIANT* p){
		if(m_pty){
			m_pty->AddRef();
			p->pdispVal = m_pty;
			p->vt = VT_DISPATCH;
		}
		else{
			p->vt = VT_EMPTY;
		}
		return S_OK;
	}
	STDMETHOD(put_Pty)(VARIANT var){
		if(m_pty){
			if(m_snapshot){
				m_pty->_del_snapshot(m_snapshot);
				m_snapshot = 0;
			}
			m_pty->Release();
			m_pty = 0;
		}
		if(var.vt == VT_DISPATCH && var.pdispVal){
			var.pdispVal->QueryInterface(__uuidof(IPty), (void**)&m_pty);
		}
		if(m_pty){
			if(m_is_active){
				m_pty->Resize(m_char_width, m_char_height);
			}
			m_notify->OnTitleInit();
		}
		draw_screen();
		return S_OK;
	}
	STDMETHOD(put_Title)(BSTR bs){ SetWindowText(m_hwnd, bs); return S_OK;}
	STDMETHOD(get_MouseLBtn)(MouseCmd* p){ *p = m_lbtn_cmd; return S_OK;}
	STDMETHOD(get_MouseMBtn)(MouseCmd* p){ *p = m_mbtn_cmd; return S_OK;}
	STDMETHOD(get_MouseRBtn)(MouseCmd* p){ *p = m_rbtn_cmd; return S_OK;}
	STDMETHOD(put_MouseLBtn)(MouseCmd n) { m_lbtn_cmd=n; return S_OK;}
	STDMETHOD(put_MouseMBtn)(MouseCmd n) { m_mbtn_cmd=n; return S_OK;}
	STDMETHOD(put_MouseRBtn)(MouseCmd n) { m_rbtn_cmd=n; return S_OK;}
	STDMETHOD(get_Color)(int i, DWORD* p){
		*p = (i < _countof(m_colors)) ? m_colors[i] : 0;
		return S_OK;
	}
	STDMETHOD(put_Color)(int i, DWORD color){
		if(i < _countof(m_colors)){
			m_colors[i] = color;
			if(i==CID_BG){
				m_graphics->SetBackgroundColor(color);
			}
		}
		return S_OK;
	}
	STDMETHOD(get_ColorAlpha)(BYTE* p){
		*p = m_bg_alpha;
		return S_OK;
	}
	STDMETHOD(put_ColorAlpha)(BYTE n){
		m_bg_alpha = n;
		return S_OK;
	}
	STDMETHOD(get_BackgroundImage)(BSTR* pp){
		BSTR path = m_graphics->GetBackgroundFile();
		*pp = (path) ? SysAllocStringLen(path, SysStringLen(path)) : 0;
		return S_OK;
	}
	STDMETHOD(put_BackgroundImage)(BSTR cygpath){
		m_graphics->SetBackgroundFile(cygpath);
		draw_screen();
		return S_OK;
	}
	STDMETHOD(get_BackgroundPlace)(DWORD* p){
		*p = m_graphics->GetBackgroundPlace();
		return S_OK;
	}
	STDMETHOD(put_BackgroundPlace)(DWORD n){
		m_graphics->SetBackgroundPlace(n);
		draw_screen();
		return S_OK;
	}
	STDMETHOD(get_FontName)(BSTR* pp){
		int len = (int)wcslen(m_font_log.lfFaceName);
		if(len<0)len=0;
		*pp = SysAllocStringLen(m_font_log.lfFaceName, len);
		return S_OK;
	}
	STDMETHOD(put_FontName)(BSTR p){ _setup_font(p, m_font_size); return S_OK;}
	STDMETHOD(get_FontSize)(float* p){ *p = m_font_size; return S_OK;}
	STDMETHOD(put_FontSize)(float n){ _setup_font(NULL, n); return S_OK;}
	STDMETHOD(get_Linespace)(BYTE* p){ *p = (BYTE)m_linespace; return S_OK;}
	STDMETHOD(put_Linespace)(BYTE n){
		if(m_linespace != n){
			m_linespace = n;
			Resize(m_char_width, m_char_height);
		}
		return S_OK;
	}
	STDMETHOD(get_Border)(DWORD* p){
		*p = (m_border.left<<24) | (m_border.top<<16) | (m_border.right<<8) | (m_border.bottom);
		return S_OK;
	}
	STDMETHOD(put_Border)(DWORD n){
		m_border.left   = (n>>24) & 0xff;
		m_border.top    = (n>>16) & 0xff;
		m_border.right  = (n>>8 ) & 0xff;
		m_border.bottom = (n>>0 ) & 0xff;
		return S_OK;
	}
	STDMETHOD(get_Scrollbar)(int* p){ *p = m_vscrl_mode; return S_OK;}
	STDMETHOD(put_Scrollbar)(int n){
		if(m_vscrl_mode != n){
			m_vscrl_mode = n;
			DWORD style   = GetWindowLong(m_hwnd, GWL_STYLE);
			DWORD exstyle = GetWindowLong(m_hwnd, GWL_EXSTYLE);
			if(n & 1) style |= WS_VSCROLL;
			else  style &= ~WS_VSCROLL;
			if(n & 2) exstyle |= WS_EX_LEFTSCROLLBAR;
			else exstyle &= ~WS_EX_LEFTSCROLLBAR;
			SetWindowLong(m_hwnd, GWL_STYLE, style);
			SetWindowLong(m_hwnd, GWL_EXSTYLE, exstyle);
			if(n & 1) SetScrollInfo(m_hwnd, SB_VERT, &m_vscrl, FALSE);
			Resize(m_char_width, m_char_height);
		}
		return S_OK;
	}
	STDMETHOD(get_Transp)(WinTransp* p){ *p = m_transp_mode; return S_OK;}
	STDMETHOD(put_Transp)(WinTransp n){
		if(m_transp_mode != n)
			m_transp_mode = Util::set_window_transp(m_hwnd,n);
		return S_OK;
	}
	STDMETHOD(get_ZOrder)(WinZOrder* p){ *p = m_zorder; return S_OK;}
	STDMETHOD(put_ZOrder)(WinZOrder n){
		if(m_zorder != n){
			m_zorder = n;
			HWND after;
			switch(m_zorder){
			case WinZOrder_Top:   after=HWND_TOPMOST;break;
			case WinZOrder_Bottom:after=HWND_BOTTOM;break;
			default:              after=HWND_NOTOPMOST;break;
			}
			SetWindowPos(m_hwnd, after,    0,0,0,0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
			SetWindowPos(m_hwnd, HWND_TOP, 0,0,0,0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
		}
		return S_OK;
	}
	STDMETHOD(get_BlinkCursor)(VARIANT_BOOL* p){ *p = m_blink_cursor; return S_OK;}
	STDMETHOD(put_BlinkCursor)(VARIANT_BOOL b){
		if(m_blink_cursor != b){
			m_blink_cursor=b;
			if(!m_blink_cursor){
				m_cursor_step = 0;
			}
		}
		return S_OK;
	}
	STDMETHOD(get_ImeState)(VARIANT_BOOL* p){ *p = m_ime_on; return S_OK;}
	STDMETHOD(put_ImeState)(VARIANT_BOOL b){
		m_ime_on = b;
		ime_set_state();
		return S_OK;
	}
	STDMETHOD(Close)(){ PostMessage(m_hwnd, WM_CLOSE, 0,0); return S_OK;}
	STDMETHOD(Show)(){ Util::show_window(m_hwnd, SW_SHOW); return S_OK;}
	STDMETHOD(Resize)(int cw, int ch){
		if(cw < 1) cw=1;
		if(ch < 1) ch=1;
		SIZE frame = get_window_frame_size(m_hwnd);
		cw = frame.cx + (m_border.left + m_border.right) + (cw * (m_font_width  + 0));
		ch = frame.cy + (m_border.top + m_border.bottom) + (ch * (m_font_height + m_linespace));
		SetWindowPos(m_hwnd,0, 0,0,cw,ch, SWP_NOMOVE|SWP_NOZORDER|SWP_NOACTIVATE);
		return S_OK;
	}
	STDMETHOD(Move)(int x, int y){
		RECT  workarea;
		SystemParametersInfo(SPI_GETWORKAREA,0,(LPVOID)&workarea,0);
		RECT rc;
		GetWindowRect(m_hwnd, &rc);
		x = (x<0) ? (workarea.right+1-(rc.right-rc.left)+x)  : (workarea.left+x);
		y = (y<0) ? (workarea.bottom+1-(rc.bottom-rc.top)+y) : (workarea.top+y);
		SetWindowPos(m_hwnd,0, x,y,0,0, SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE);
		return S_OK;
	}
	STDMETHOD(PopupMenu)(VARIANT_BOOL system_menu){
		if(system_menu)
			PostMessage(m_hwnd, WM_SYSCOMMAND, SC_KEYMENU, VK_SPACE);
		else
			PostMessage(m_hwnd, WM_CONTEXTMENU, 0,MAKELPARAM(-1,-1));
		return S_OK;
	}
};


//----------------------------------------------------------------------------

void Window_::draw_text(RECT& rect, CharFlag style, UINT32* unicodeText, FLOAT* unicodeStep, UINT32 unicodeLen){
	DWORD colorFG, colorBG;
	int   cidFG = (style & CharFlag_FG) ? ((style>>16)&0xFF) : CID_FG;
	int   cidBG = (style & CharFlag_BG) ? ((style>>24)&0xFF) : CID_BG;
	if(style & CharFlag_Invert){
		int  bak = cidFG;
		cidFG = cidBG;
		cidBG = bak;
	}
	if(style & CharFlag_Bold){
		if(cidFG <= 7)
			cidFG += 8;
	}

	colorFG = (m_colors[cidFG]  & 0x00FFFFFF) | 0xFF000000;
	colorBG = (m_colors[CID_BG] & 0x00FFFFFF);
	if(cidBG != CID_BG){
		colorBG = composite_argb(colorBG, (m_colors[cidBG] & 0x00FFFFFF) | (m_bg_alpha<<24));
	}
	if(style & CharFlag_Select){
		colorFG = composite_argb(colorFG, m_colors[CID_Select]);
		colorBG = composite_argb(colorBG, m_colors[CID_Select]);
	}
	if(style & CharFlag_Cursor){
		UINT  alpha = m_cursor_step;
		if(!(alpha & 0x20)) alpha = ~alpha;
		alpha &= 0x1F;
		alpha = (alpha<<3) | (alpha>>2);

		DWORD  c = (m_colors[ m_ime_on ? CID_ImeCursor : CID_Cursor ] & 0x00FFFFFF) | (alpha<<24);
		DWORD  cursorFG = composite_argb(colorFG, c ^ 0x00FFFFFF);
		DWORD  cursorBG = composite_argb(colorBG, c);
		if(style & CharFlag_Select){
			cursorFG = composite_argb(cursorFG, m_colors[CID_Select]);
			cursorBG = composite_argb(cursorBG, m_colors[CID_Select]);
		}
		if(m_is_active){
			if(m_blink_cursor) m_cursor_step++;
			colorFG = cursorFG;
			m_graphics->FillBackground(rect, cursorBG);
		}else{
			m_graphics->FillBackground(rect, colorBG);
			m_graphics->DrawRect(rect, cursorBG);
		}
	}else{
		m_graphics->FillBackground(rect, colorBG);
	}

	//

	float  em2pixelX, em2pixelY;
	m_graphics->GetDesktopDpi(&em2pixelX, &em2pixelY);
	em2pixelX = (em2pixelX/72.0f) * m_font_size;
	em2pixelY = (em2pixelY/72.0f) * m_font_size;

	D2D1_POINT_2F  position;
	position.x = float(rect.left);
	position.y = float(rect.top) + (m_fontManager.GetBaseline()*em2pixelY);

	DWRITE_GLYPH_OFFSET*   glyphOffsets = &m_unicode_glyphoffsets[0];
	UINT16*  glyphIndices = &m_unicode_glyphindices[0];
	float    glyphWidth;
	float    unicodeWidth = unicodeStep[0];
	UINT32   prevStart = 0;
	IDWriteFontFace*  face;
	IDWriteFontFace*  prevFace;

	prevFace = m_fontManager.GetGlyph( unicodeText[0], style, &glyphIndices[0], &glyphWidth );
	glyphOffsets[0].advanceOffset  = (unicodeStep[0] - (glyphWidth*em2pixelY)) * 0.5f;
	glyphOffsets[0].ascenderOffset = 0.0f;

	for(UINT32 i=1; i < unicodeLen; ++i){
		face = m_fontManager.GetGlyph( unicodeText[i], style, &glyphIndices[i], &glyphWidth );
		if(prevFace != face){
			if(prevFace){
				DWRITE_GLYPH_RUN  glyphRun = { prevFace, em2pixelY, i-prevStart, glyphIndices+prevStart, unicodeStep+prevStart, glyphOffsets+prevStart, FALSE, 0 };
				m_graphics->DrawGlyphRun(position, &glyphRun, colorFG);
			}
			prevFace  = face;
			prevStart = i;
			position.x += unicodeWidth;
			unicodeWidth = 0.0f;
		}
		glyphOffsets[i].advanceOffset  = (unicodeStep[i] - (glyphWidth*em2pixelY)) * 0.5f;
		glyphOffsets[i].ascenderOffset = 0.0f;
		unicodeWidth += unicodeStep[i];
	}
	if(prevFace){
		DWRITE_GLYPH_RUN  glyphRun = { prevFace, em2pixelY, unicodeLen-prevStart, glyphIndices+prevStart, unicodeStep+prevStart, glyphOffsets+prevStart, FALSE, 0 };
		m_graphics->DrawGlyphRun(position, &glyphRun, colorFG);
	}
	position.x += unicodeWidth;

	if(style & CharFlag_Uline){
		D2D1_POINT_2F  p0, p1;
		p0.x = float(rect.left);
		p1.x = position.x;
		p0.y = \
		p1.y = std::floor( position.y + (m_fontManager.GetUnderline()*em2pixelY) + 0.5f ) + 0.5f; // pos = round( Baseline + Underline ) + PixelCenter
		m_graphics->DrawLine(p0, p1, colorFG);
	}
}


void Window_::draw_screen(){
	if(!m_pty) return;

	Snapshot*  nowSnapshot = m_snapshot;
	Snapshot*  oldSnapshot = 0;

	int  drawMode = 0;//cursor only
	{
		int  tick;
		m_pty->_screen_tick(&tick);
		if(!nowSnapshot || m_screen_tick != tick){
			m_screen_tick = tick;
			m_pty->_new_snapshot(&nowSnapshot);
			if(nowSnapshot){
				oldSnapshot = m_snapshot;
				m_snapshot = nowSnapshot;
			}
			if(!oldSnapshot ||
			   oldSnapshot->width  != nowSnapshot->width ||
			   oldSnapshot->height != nowSnapshot->height){
				drawMode = 2;//full
			}else{
				drawMode = 1;//diff
			}
		}
	}
	if(!nowSnapshot){
		return;
	}

	int grstate = m_graphics->BeginDraw();
	if(grstate != 0){
		if(grstate == 2) drawMode=2;

		const Char*  nowChars = nowSnapshot->chars;
		const Char*  oldChars = (drawMode==1) ? oldSnapshot->chars : 0;

		const int  maxCW = (m_char_width  < nowSnapshot->width)  ? m_char_width  : nowSnapshot->width;
		const int  maxCH = (m_char_height < nowSnapshot->height) ? m_char_height : nowSnapshot->height;
		const int  lineH = m_font_height + m_linespace;

		m_unicode_text.reserve(maxCW);
		m_unicode_step.reserve(maxCW);
		m_unicode_glyphindices.reserve(maxCW);
		m_unicode_glyphoffsets.reserve(maxCW);

		UINT32*  ws = &m_unicode_text[0]; 
		float*   wi = &m_unicode_step[0]; 
		UINT32   wn = 0;
		CharFlag style = (CharFlag)0;
		RECT     rc;

		if(drawMode==0){
			const int   x = nowSnapshot->cursor_x;
			const int   y = nowSnapshot->cursor_y;
			if(x>=0){
				rc.left   = m_border.left + (m_font_width * x);
				rc.top    = m_border.top  + (m_linespace>>1) + (lineH * y);
				rc.right  = rc.left + m_font_width;
				rc.bottom = rc.top  + m_font_height;

				const Char&  c = nowChars[nowSnapshot->width * y + x];
				style = (CharFlag)(c.flags & CharFlag_Styles);

				wn = 1;
				ws[0] = (c.ch < 0x20) ? ' ' : c.ch;

				if(c.flags & CharFlag_MbLead){
					rc.right += m_font_width;
					wi[0] = float(m_font_width<<1);
				}
				else if(c.flags & CharFlag_MbTrail){
					rc.left -= m_font_width;
					wi[0] = float(m_font_width<<1);
				}
				else{
					wi[0] = float(m_font_width);
				}

				m_graphics->PushClipRect(rc);
				draw_text(rc, style, ws, wi, wn);
				m_graphics->PopClipRect();
			}
		}
		else{
			rc.top = 0 + m_border.top + (m_linespace>>1);

			for(int y=0; y < maxCH; y++){
				if(!oldChars || memcmp(oldChars, nowChars, sizeof(Char) * maxCW) != 0){
					rc.bottom = rc.top + m_font_height;
					rc.left   = m_border.left;
					rc.right  = m_graphics->width() - m_border.right;

					m_graphics->PushClipRect(rc);

					rc.right = rc.left;

					for(int x=0; x < maxCW; x++){
						const Char& c = nowChars[x];

						if(c.flags & CharFlag_MbTrail){
							if(x>0)continue;
							rc.left = rc.right -= m_font_width;
						}
						CharFlag s = (CharFlag)(c.flags & CharFlag_Styles);
						if(style != s){
							if(wn > 0){
								draw_text(rc, style, ws, wi, wn);
								wn=0;
								rc.left=rc.right;
							}
							style = s;
						}

						int  charWidth = (c.flags & (CharFlag_MbLead|CharFlag_MbTrail)) ? m_font_width<<1 : m_font_width;
						ws[wn] = (c.ch < 0x20 || c.ch==0x7F) ? ' ' : c.ch;
						wi[wn] = float(charWidth);
						rc.right += charWidth;
						wn++;
					}
					if(wn > 0){
						draw_text(rc, style, ws, wi, wn);
						wn=0;
					}

					m_graphics->PopClipRect();
				}

				rc.top += lineH;
				if(oldChars) oldChars += nowSnapshot->width;
				nowChars += nowSnapshot->width;
			}
		}

		m_graphics->EndDraw();
	}

	if(oldSnapshot){
		m_pty->_del_snapshot(oldSnapshot);
	}

	if(m_vscrl.nPage != nowSnapshot->height ||
	   m_vscrl.nPos  != nowSnapshot->view ||
	   m_vscrl.nMax  != nowSnapshot->nlines-1){
		m_vscrl.nPage = nowSnapshot->height;
		m_vscrl.nPos  = nowSnapshot->view;
		m_vscrl.nMax  = nowSnapshot->nlines-1;
		if(m_vscrl_mode & 1){
			SetScrollInfo(m_hwnd,SB_VERT,&m_vscrl,TRUE);
		}
	}
}



//----------------------------------------------------------------------------

class Window: public IDispatchImpl3<IWnd>, public IWindowNotify_{
protected:
	Window_*  m_obj;
	IWndNotify* const  m_notify;
	//
	void _finalize(){
		if(m_obj){ delete m_obj; m_obj=0; }
	}
	virtual ~Window(){
		trace("Window::dtor\n");
		_finalize();
	}
public:
	Window(IWndNotify* cb): m_obj(0), m_notify(cb){
		trace("Window::ctor\n");
		m_obj = new Window_(this);
	}

	//STDMETHOD_(ULONG,AddRef)(){ ULONG n=IDispatchImpl3<IWnd>::AddRef(); trace("Window::AddRef %d\n", n); return n;}
	//STDMETHOD_(ULONG,Release)(){ ULONG n=IDispatchImpl3<IWnd>::Release(); trace("Window::Release %d\n", n); return n;}

	STDMETHOD(OnClosed)(){ return m_notify->OnClosed(this);}
	STDMETHOD(OnKeyDown)(DWORD vk){ return m_notify->OnKeyDown(this, vk);}
	STDMETHOD(OnMouseWheel)(int delta){ return m_notify->OnMouseWheel(this,delta);}
	STDMETHOD(OnMenuInit)(HMENU menu){ return m_notify->OnMenuInit(this, (int*)menu);}
	STDMETHOD(OnMenuExec)(DWORD id){ return m_notify->OnMenuExec(this, id);}
	STDMETHOD(OnTitleInit)(){ return m_notify->OnTitleInit(this);}
	STDMETHOD(OnDrop)(BSTR bs, int type, DWORD key){ return m_notify->OnDrop(this,bs,type,key);}
	STDMETHOD(OnPasteClipboard)(){ return m_notify->OnPasteClipboard(this);}
	//
	STDMETHOD(Dispose)(){
		trace("Window::dispose\n");
		_finalize();
		return S_OK;
	}
	//
	STDMETHOD(get_Handle)(UINT* p){ return m_obj->get_Handle(p);}
	STDMETHOD(get_Pty)(VARIANT* p){ return m_obj->get_Pty(p);}
	STDMETHOD(put_Pty)(VARIANT var){ return m_obj->put_Pty(var);}
	STDMETHOD(put_Title)(BSTR p){ return m_obj->put_Title(p);}
	STDMETHOD(get_MouseLBtn)(MouseCmd* p){ return m_obj->get_MouseLBtn(p);}
	STDMETHOD(put_MouseLBtn)(MouseCmd n) { return m_obj->put_MouseLBtn(n);}
	STDMETHOD(get_MouseMBtn)(MouseCmd* p){ return m_obj->get_MouseMBtn(p);}
	STDMETHOD(put_MouseMBtn)(MouseCmd n) { return m_obj->put_MouseMBtn(n);}
	STDMETHOD(get_MouseRBtn)(MouseCmd* p){ return m_obj->get_MouseRBtn(p);}
	STDMETHOD(put_MouseRBtn)(MouseCmd n) { return m_obj->put_MouseRBtn(n);}
	STDMETHOD(get_Color)(int i, DWORD* p){ return m_obj->get_Color(i,p);}
	STDMETHOD(put_Color)(int i, DWORD color){ return m_obj->put_Color(i,color);}
	STDMETHOD(get_ColorAlpha)(BYTE* p){ return m_obj->get_ColorAlpha(p);}
	STDMETHOD(put_ColorAlpha)(BYTE n){ return m_obj->put_ColorAlpha(n);}
	STDMETHOD(get_BackgroundImage)(BSTR* pp){ return m_obj->get_BackgroundImage(pp);}
	STDMETHOD(put_BackgroundImage)(BSTR path){ return m_obj->put_BackgroundImage(path);}
	STDMETHOD(get_BackgroundPlace)(DWORD* p){ return m_obj->get_BackgroundPlace(p);}
	STDMETHOD(put_BackgroundPlace)(DWORD n){ return m_obj->put_BackgroundPlace(n);}
	STDMETHOD(get_FontName)(BSTR* pp){ return m_obj->get_FontName(pp);}
	STDMETHOD(put_FontName)(BSTR p){ return m_obj->put_FontName(p);}
	STDMETHOD(get_FontSize)(float* p){ return m_obj->get_FontSize(p);}
	STDMETHOD(put_FontSize)(float n){ return m_obj->put_FontSize(n);}
	STDMETHOD(get_LineSpace)(BYTE* p){ return m_obj->get_Linespace(p);}
	STDMETHOD(put_LineSpace)(BYTE n){ return m_obj->put_Linespace(n);}
	STDMETHOD(get_Border)(DWORD* p){ return m_obj->get_Border(p);}
	STDMETHOD(put_Border)(DWORD n){ return m_obj->put_Border(n);}
	STDMETHOD(get_Scrollbar)(int* p){ return m_obj->get_Scrollbar(p);}
	STDMETHOD(put_Scrollbar)(int n){ return m_obj->put_Scrollbar(n);}
	STDMETHOD(get_Transp)(WinTransp* p){ return m_obj->get_Transp(p);}
	STDMETHOD(put_Transp)(WinTransp n) { return m_obj->put_Transp(n);}
	STDMETHOD(get_ZOrder)(WinZOrder* p){ return m_obj->get_ZOrder(p);}
	STDMETHOD(put_ZOrder)(WinZOrder n) { return m_obj->put_ZOrder(n);}
	STDMETHOD(get_BlinkCursor)(VARIANT_BOOL* p){ return m_obj->get_BlinkCursor(p);}
	STDMETHOD(put_BlinkCursor)(VARIANT_BOOL b) { return m_obj->put_BlinkCursor(b);}
	STDMETHOD(get_ImeState)(VARIANT_BOOL* p){ return m_obj->get_ImeState(p);}
	STDMETHOD(put_ImeState)(VARIANT_BOOL b) { return m_obj->put_ImeState(b);}
	STDMETHOD(Close)(){ return m_obj->Close();}
	STDMETHOD(Show)(){ return m_obj->Show();}
	STDMETHOD(Resize)(int cw, int ch){ return m_obj->Resize(cw,ch);}
	STDMETHOD(Move)(int x, int y){ return m_obj->Move(x,y);}
	STDMETHOD(PopupMenu)(VARIANT_BOOL system_menu){ return m_obj->PopupMenu(system_menu);}
};


}//namespace Ck

//----------------------------------------------------------------------------

extern "C" __declspec(dllexport) HRESULT CreateWnd(Ck::IWndNotify* cb, Ck::IWnd** pp){
	if(!pp) return E_POINTER;
	if(!cb) return E_INVALIDARG;

	HRESULT hr;
	Ck::Window* w = 0;

	try{
		w = new Ck::Window(cb);
		w->AddRef();
		hr = S_OK;
	}
	catch(HRESULT e){
		hr = e;
	}
	catch(std::bad_alloc&){
		hr = E_OUTOFMEMORY;
	}
	catch(...){
		hr = E_FAIL;
	}

	(*pp) = w;
	return hr;
}

//EOF
